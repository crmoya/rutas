<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Ciudad;
use app\models\Tipo;

/* @var $this yii\web\View */
/* @var $model app\models\Lugar */
/* @var $form yii\widgets\ActiveForm */
?>

<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />    
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>

<div class="lugar-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?php $ciudades=ArrayHelper::map(Ciudad::find()->asArray()->all(), 'id', 'nombre'); ?>
            <?= $form->field($model, 'ciudad_id')->dropDownList($ciudades,['prompt'=>'Seleccione Ciudad']); ?>
        </div>
        <div class="col-md-4">
            <?php $tipos=ArrayHelper::map(Tipo::find()->asArray()->all(), 'id', 'nombre'); ?>
            <?= $form->field($model, 'tipo_id')->dropDownList($tipos,['prompt'=>'Seleccione Tipo']); ?>
        </div>
    </div>  

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'descripcion')->textarea(['rows' => 6]) ?>
        </div>
        <div class="col-md-6">
            <?= Html::activeLabel($model, 'localización'); ?>
            <div id="map"></div>
            <?= $form->field($model,'latitud')->hiddenInput()->label(false);?>
            <?= $form->field($model,'longitud')->hiddenInput()->label(false);?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'facebook') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'instagram') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'web') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
#map{
    width:100%;
    height:250px;
}
#lugar-descripcion{
    height:250px;
}
</style>


<?php
$latitud = 0;
$longitud = 0;

if(isset($model->latitud) && isset($model->longitud))
{
    $latitud = $model->latitud;
    $longitud = $model->longitud;
}
else{
    $latitud = -40.5;
    $longitud = -73.1;
}
?>

<script>
    /**
    * Adds a  draggable marker to the map..
    *
    * @param {H.Map} map                      A HERE Map instance within the
    *                                         application
    * @param {H.mapevents.Behavior} behavior  Behavior implements
    *                                         default interactions for pan/zoom
    */
    var latitud = <?=$latitud?>;
    var longitud = <?=$longitud?>;
    var zoom = 15;



        
    function addDraggableMarker(map, behavior){

        var marker = new H.map.Marker({lat:latitud, lng:longitud}, {
            // mark the object as volatile for the smooth dragging
            volatility: true
        });
        // Ensure that the marker can receive drag events
        marker.draggable = true;
        map.addObject(marker);

        // disable the default draggability of the underlying map
        // and calculate the offset between mouse and target's position
        // when starting to drag a marker object:
        map.addEventListener('dragstart', function(ev) {
            var target = ev.target,
            pointer = ev.currentPointer;
            if (target instanceof H.map.Marker) {
                var targetPosition = map.geoToScreen(target.getGeometry());
                target['offset'] = new H.math.Point(pointer.viewportX - targetPosition.x, pointer.viewportY - targetPosition.y);
                behavior.disable();
            }
        }, false);


        // re-enable the default draggability of the underlying map
        // when dragging has completed
        map.addEventListener('dragend', function(ev) {
            var target = ev.target;            
            var lat = target.b["lat"];
            var lon = target.b["lng"];
            var inputLat = document.getElementById("lugar-latitud");
            var inputLon = document.getElementById("lugar-longitud");
            inputLat.value = lat;
            inputLon.value = lon;
            if (target instanceof H.map.Marker) {
                behavior.enable();
            }
        }, false);

        // Listen to the drag event and move the position of the marker
        // as necessary
        map.addEventListener('drag', function(ev) {
            var target = ev.target,
            pointer = ev.currentPointer;
            if (target instanceof H.map.Marker) {
                target.setGeometry(map.screenToGeo(pointer.viewportX - target['offset'].x, pointer.viewportY - target['offset'].y));
            }
        }, false);
    }

    /**
    * Boilerplate map initialization code starts below:
    */

    //Step 1: initialize communication with the platform
    // In your own code, replace variable window.apikey with your own apikey
    var platform = new H.service.Platform({
        apikey: 'N-YZ6I_nuKSPWOfumH-EimqlpC82j0uM_4bzmhEEpnw'
    });
    var defaultLayers = platform.createDefaultLayers();

    //Step 2: initialize a map - this map is centered over Boston
    var map = new H.Map(document.getElementById('map'),
    defaultLayers.vector.normal.map, {
        center: {lat:latitud, lng:longitud},
        zoom: zoom,
        pixelRatio: window.devicePixelRatio || 1
    });
    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => map.getViewPort().resize());

    //Step 3: make the map interactive
    // MapEvents enables the event system
    // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

    // Step 4: Create the default UI:
    var ui = H.ui.UI.createDefault(map, defaultLayers, 'en-US');

    // Add the click event listener.
    addDraggableMarker(map, behavior);

</script>