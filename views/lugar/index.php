<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LugarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lugares';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lugar-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Lugar', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'nombre',
            [
                'attribute'=>'ciudad',
                'value' => function ($model) {
                    return $model->ciudad->nombre;
                },
            ],
            [
                'attribute'=>'tipo',
                'value' => function ($model) {
                    return $model->tipo->nombre;
                },
            ], 
            [
                'attribute'=>'rutas',
                'value' => function ($model) {
                    return $model->rutas;
                },
            ],  
            'descripcion:ntext',
            //'latitud',
            //'longitud',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
