<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Lugar */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Lugares', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid gr">
    <div class="row">
        <div class="col-md-8 container-galeria">
            <table class="galeria">
                <tr>
                    <td>
                        <img width="355px" height="355px" src="<?=Yii::getAlias("@web")?>/images/lugares/<?=$model->id?>/1.jpg"/>   
                    </td>
                    <td>
                        <table class="miniaturas">
                            <tr>
                                <td>
                                    <img width="175px" height="175px" src="<?=Yii::getAlias("@web")?>/images/lugares/<?=$model->id?>/2.jpg"/>   
                                </td>
                                <td>
                                    <img width="175px" height="175px" src="<?=Yii::getAlias("@web")?>/images/lugares/<?=$model->id?>/3.jpg"/>   
                                </td>
                            </tr> 
                            <tr>
                                <td>
                                    <img width="175px" height="175px" src="<?=Yii::getAlias("@web")?>/images/lugares/<?=$model->id?>/4.jpg"/>   
                                </td>
                                <td>
                                    <img width="175px" height="175px" src="<?=Yii::getAlias("@web")?>/images/lugares/<?=$model->id?>/5.jpg"/>   
                                </td>
                            </tr>  
                        </table> 
                    </td>
                </tr>  
                <tr>
                    <td colspan="2">
                        <img class="linea" width="100%" src="<?=Yii::getAlias("@web")?>/images/linea.png"/>   
                    </td>
                </tr> 
            </table>
        </div>
        <div class="col-md-4">
            <table class="votaciones">
                <tr>
                    <td>
                        <div class="rombo"></div>
                        <div class="rombo"></div>
                        <div class="rombo"></div>
                        <div class="rombo"></div>
                        <div class="rombo"></div>
                        <div class="rombo"></div>
                        <div class="rombo-gris"></div>
                    </td>
                </tr> 
                <tr>
                    <td style="text-align:center;">
                        2.135 votaciones
                    </td>
                </tr>
                <tr>
                    <td>
                        <table class="calificaciones">
                            <tr>
                                <td>
                                    <b>Calificaciones Según:</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img class="linea" width="100%" src="<?=Yii::getAlias("@web")?>/images/linea.png"/>   
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>LIMPIEZA</b><br/>
                                    <div class="outer-votos">
                                        <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </div>    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>PERSONAL</b><br/>
                                    <div class="outer-votos">
                                        <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </div>    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>UBICACIÓN</b><br/>
                                    <div class="outer-votos">
                                        <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </div>    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>COMODIDAD</b><br/>
                                    <div class="outer-votos">
                                        <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </div>    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>INSTALACIONES Y SERVICIOS</b><br/>
                                    <div class="outer-votos">
                                        <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </div>    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>RELACIÓN CALIDAD / PRECIO</b><br/>
                                    <div class="outer-votos">
                                        <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    </div>    
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br/>    
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr> 
            </table> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <b class="titulo"><?=$model->nombre?></b>
        </div>        
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                    <b>Ciudad:</b> <?=$model->ciudad->nombre?>
                </div>
                <div class="col-md-8">
                    <?=isset($model->facebook) && $model->facebook != ""?'<span class="link"><a target="_blank" href="'.$model->facebook.'"><img class="img-link" src="'.Yii::getAlias("@web").'/images/fb.png"></a></span>':''?>
                    <?=isset($model->instagram) && $model->instagram != ""?'<span class="link"><a target="_blank" href="'.$model->instagram.'"><img class="img-link" src="'.Yii::getAlias("@web").'/images/inst.png"></a></span>':''?>
                    <?=isset($model->web) && $model->web != ""?'<span class="link"><a target="_blank" href="'.$model->web.'"><img class="img-link" src="'.Yii::getAlias("@web").'/images/web.png"></a></span>':''?>
                </div>
            </div>
            <div class="row">
                <?php foreach($model->lugarCaracteristicas as $lugarCaracteristica){
                    echo "<b>".$lugarCaracteristica->caracteristica->nombre."</b>".": ".$lugarCaracteristica->valor."<br/>";
                }?>
            </div>
            <div class="row" style="text-align: justify;border:1px solid silver; padding: 4px;">
                <br/>
                <?=$model->descripcion?>
            </div>
        </div>       
    </div>
</div>

<div class="container-fluid ch">
    <div class="row">
        <div class="col bordes borde-sup">
            <img class="imagen-ppal" src="<?=Yii::getAlias("@web")?>/images/lugares/<?=$model->id?>/1.jpg"/>   
        </div>
    </div>
    <div class="row">
        <div class="col bordes">
        <img class="linea" width="100%" src="<?=Yii::getAlias("@web")?>/images/linea.png"/>           
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 bordes tit">
            <b class="titulo"><?=$model->nombre?></b>
        </div>        
    </div>
    <div class="row">
        <table style="width:90%;margin-left:5%;margin-bottom:10px;">
            <tr>
                <td>
                    <b>Ciudad:</b> <?=$model->ciudad->nombre?><br/>
                    <?php foreach($model->lugarCaracteristicas as $lugarCaracteristica){
                        echo "<b>".$lugarCaracteristica->caracteristica->nombre."</b>".": ".$lugarCaracteristica->valor."<br/>";
                    }?>
                </td>
                <td style="text-align:right;">
                    <?=isset($model->facebook) && $model->facebook != ""?'<span class="link"><a target="_blank" href="'.$model->facebook.'"><img class="img-link" src="'.Yii::getAlias("@web").'/images/fb.png"></a></span>':''?>
                    <?=isset($model->instagram) && $model->instagram != ""?'<span class="link"><a target="_blank" href="'.$model->instagram.'"><img class="img-link" src="'.Yii::getAlias("@web").'/images/inst.png"></a></span>':''?>
                    <?=isset($model->web) && $model->web != ""?'<span class="link"><a target="_blank" href="'.$model->web.'"><img class="img-link" src="'.Yii::getAlias("@web").'/images/web.png"></a></span>':''?>
                </td>
            </tr>
        </table> 
    </div> 
    <div style="width:96%;margin-left:2%;text-align: justify;border:1px solid silver; padding: 4px;">
        <?=$model->descripcion?>
    </div>
    <div class="row vot-ch">
        <div class="col-xs-4">2.135 votaciones</div>
        <div class="col-xs-8">
            <div class="rombo"></div>
            <div class="rombo"></div>
            <div class="rombo"></div>
            <div class="rombo"></div>
            <div class="rombo"></div>
            <div class="rombo"></div>
            <div class="rombo-gris"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 calif-ch">
            <table class="calificaciones-ch">
                <tr>
                    <td>
                        <b>Calificaciones Según:</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img class="linea" width="100%" src="<?=Yii::getAlias("@web")?>/images/linea.png"/>   
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>LIMPIEZA</b><br/>
                        <div class="outer-votos">
                            <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>PERSONAL</b><br/>
                        <div class="outer-votos">
                            <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>UBICACIÓN</b><br/>
                        <div class="outer-votos">
                            <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>COMODIDAD</b><br/>
                        <div class="outer-votos">
                            <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>INSTALACIONES Y SERVICIOS</b><br/>
                        <div class="outer-votos">
                            <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>    
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>RELACIÓN CALIDAD / PRECIO</b><br/>
                        <div class="outer-votos">
                            <span class="votos">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        </div>  
                        <br/>  
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="mapa-ch" id="map" style="height:250px;"></div>
        </div>
    </div>
</div>



<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
<script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
<script>

var lat = <?=$model->latitud?>;
var lng = <?=$model->longitud?>;
var zoom = 14;
var platform = new H.service.Platform({
    'apikey': 'N-YZ6I_nuKSPWOfumH-EimqlpC82j0uM_4bzmhEEpnw'
});
var defaultLayers = platform.createDefaultLayers();
var map = new H.Map(document.getElementById('map'),
    defaultLayers.vector.normal.map,{
        center: {lat:lat, lng:lng},
        zoom: zoom,
        pixelRatio: window.devicePixelRatio || 1
    }
);
var ui = H.ui.UI.createDefault(map, defaultLayers);
window.addEventListener('resize', () => map.getViewPort().resize());
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

var icon = new H.map.Icon("<?=Yii::getAlias("@web")."/images/iconos/"?>marker.png");        
var lugar = new H.map.Marker({
    lat: '<?=$model->latitud;?>',
    lng: '<?=$model->longitud;?>'
},{
    icon: icon
});
map.addObject(lugar);
</script>