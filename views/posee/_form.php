<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Lugar;
use app\models\Emprendedor;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Posee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posee-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $lugares=ArrayHelper::map(Lugar::find()->asArray()->all(), 'id', 'nombre'); ?>
    <?= $form->field($model, 'lugar_id')->dropDownList($lugares,['prompt'=>'Seleccione Lugar']); ?>

    <?php 
    $emprendedor = new Emprendedor();
    $emprendedores=ArrayHelper::map($emprendedor->findUsuarios(), 'id', 'nombre'); ?>
    <?= $form->field($model, 'emprendedor_id')->dropDownList($emprendedores,['prompt'=>'Seleccione Emprendedor']); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
