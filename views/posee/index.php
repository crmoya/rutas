<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PoseeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Posees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posee-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Posee', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'lugar_id',
                'value' => function ($model) {
                    return $model->lugar->nombre;
                },
            ], 
            [
                'attribute'=>'emprendedor_id',
                'value' => function ($model) {
                    return $model->emprendedor->usuario->nombre." ".$model->emprendedor->usuario->apellido;
                },
            ], 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
