<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Posee */

$this->title = 'Crear Posee';
$this->params['breadcrumbs'][] = ['label' => 'Posees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posee-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
