<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="shortcut icon" href="<?=Yii::getAlias("@web")?>/images/icono.png" type="image/x-icon">
    <link rel="icon" href="<?=Yii::getAlias("@web")?>/images/icono.png" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <link href="<?=Yii::getAlias("@web")?>/css/transform.css" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="<?=Yii::getAlias("@web")?>/css/lugar.css" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container-fluid">
    <div class="container-fluid">
        <div class="row rowmenu">
            <div class="col-xs-8">
                <a href='<?=Url::to(['site/index'])?>'>
                    <img src="<?=Yii::getAlias("@web")?>/images/icono.png"/>
                </a>
            </div>
            <div class="menu col-xs-4 ch">
                <img class="sandwich" src="<?=Yii::getAlias("@web")?>/images/sandwich.png"/>
            </div>
            <div class="menu col-md-4 nopadding mdgr">
                <span class="over-menu"><a class="no-decoration" href="<?=Url::to(['site/galeria'])?>">Galería de Imágenes</a></span>
            </div>
        </div>
    </div>
    <ul class="abierto" style="display:none;">
        <li><a class="no-decoration" href="<?=Url::to(['site/galeria'])?>">Galería de Imágenes</a></li>
    </ul>
    <?= Alert::widget() ?>
    <?= $content ?>
    <div class="pie">
        <img class='img-footer' src="<?=Yii::getAlias("@web")?>/images/linea.png"/>
        <table class="logos">
            <tr>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/inacap.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/corporacion.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/sercotec.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/municipalidad.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/rutas.png"/></td>
            </tr>
        </div>        
    </div>     
</div>


<?php 

$script = <<< JS

$(document).ready(function(e){

    var abierto = false;
    $('.sandwich').click(function(e){
        if(!abierto){
            $('.abierto').show();
        }
        else{
            $('.abierto').hide();   
        }
        abierto = !abierto;
    });

});
        
JS;
$this->registerJs($script);


$this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
