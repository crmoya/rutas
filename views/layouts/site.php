<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?=Yii::getAlias("@web")?>/images/icono.png" type="image/x-icon">
    <link rel="icon" href="<?=Yii::getAlias("@web")?>/images/icono.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="<?=Yii::getAlias("@web")?>/css/alternate-site.css" rel="stylesheet">
    <link href="<?=Yii::getAlias("@web")?>/css/transform.css" rel="stylesheet">
</head>
<body>
<div id="mask" style="height:100%;width:100%;position: absolute; top: 0; left: 0; background: black; display: none;"​​​​​​​​​​​​​​​​​​​​​​></div>
<?php $this->beginBody() ?>

<div class="container-fluid">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    
    <div class="container-fluid">
        <div class="row rowmenu">
            <div class="col-xs-8">
                <a href='<?=Url::to(['site/index'])?>'>
                    <img src="<?=Yii::getAlias("@web")?>/images/icono.png"/>
                </a>
            </div>
            <div class="menu col-xs-4 ch">
                <img class="sandwich" src="<?=Yii::getAlias("@web")?>/images/sandwich.png"/>
            </div>
            <div class="menu col-md-4 nopadding mdgr">
                <span class="over-menu"><a class="no-decoration" href="<?=Url::to(['site/galeria'])?>">Galería de Imágenes</a></span>
            </div>
        </div>
    </div>
    <ul class="abierto" style="display:none;">
        <li><a class="no-decoration" href="<?=Url::to(['site/galeria'])?>">Galería de Imágenes</a></li>
    </ul>
    <div class="cabecera ch"></div> 
    <img class="ch linea-ch" src="<?=Yii::getAlias("@web")?>/images/linea.png"/>
    <div class="medio"> 
        <?= $content ?>
    </div>
    <div class="pie">
        <img class='img-footer' src="<?=Yii::getAlias("@web")?>/images/linea.png"/>
        <table class="logos">
            <tr>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/inacap.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/corporacion.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/sercotec.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/municipalidad.png"/></td>
                <td><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/rutas.png"/></td>
            </tr>
        </div>        
    </div>
</div>

<?php
$url = Yii::getAlias("@web");
$script = <<< JS

$(document).ready(function(e){

    var abierto = false;
    $('.sandwich').click(function(e){
        if(!abierto){
            $('.abierto').show();
        }
        else{
            $('.abierto').hide();   
        }
        abierto = !abierto;
    });


    var width = $(window).width();
    if(width > 479){
        var backgrounds = new Array(
              'url("$url/images/fondo1.jpg")'
            , 'url("$url/images/fondo2.jpg")'
            , 'url("$url/images/fondo3.jpg")'
            , 'url("$url/images/fondo0.jpg")'
        );

        var current = 0;
        var i = Math.floor(Math.random()*4);
        $('body').css('background-image', backgrounds[i]);
    }
    

});
        
JS;
$this->registerJs($script);
?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

