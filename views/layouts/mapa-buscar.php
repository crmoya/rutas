<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <link rel="shortcut icon" href="<?=Yii::getAlias("@web")?>/images/icono.png" type="image/x-icon">
    <link rel="icon" href="<?=Yii::getAlias("@web")?>/images/icono.png" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="<?=Yii::getAlias("@web")?>/css/mapa.css" rel="stylesheet">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container-fluid">
    <div class="container-fluid">
        <div class="row rowmenu">
            <div class="col-md-8">
                <a href='<?=Url::to(['site/index'])?>'><img src="<?=Yii::getAlias("@web")?>/images/icono.png"/></a>
            </div>
            <div class="col-md-4 nopadding menu">
                <span class="over-menu"><a class="no-decoration" href="<?=Url::to(['site/galeria'])?>">Galería de Imágenes</a></span>
                <span class="over-menu">Ayuda</span>
            </div>
        </div>
    </div>
    <?= Alert::widget() ?>
    <?= $content ?>
    <div class="pie">
        <img class='img-footer' src="<?=Yii::getAlias("@web")?>/images/linea.png"/>
        <div class="logos">
            <span><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/inacap.png"/></span>
            <span><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/corporacion.png"/></span>
            <span><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/sercotec.png"/></span>
            <span><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/municipalidad.png"/></span>
            <span><img class="img-logo" src="<?=Yii::getAlias("@web")?>/images/rutas.png"/></span>
        </div>        
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
