<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Ruta;

/* @var $this yii\web\View */
/* @var $model app\models\Punto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="punto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'orden')->textInput() ?>

    <?= $form->field($model, 'latitud')->textInput() ?>

    <?= $form->field($model, 'longitud')->textInput() ?>

    <?php $rutas=ArrayHelper::map(Ruta::find()->asArray()->all(), 'id', 'nombre'); ?>
    <?= $form->field($model, 'ruta_id')->dropDownList($rutas,['prompt'=>'Seleccione Ruta']); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
