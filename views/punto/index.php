<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PuntoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Puntos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="punto-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Punto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'orden',
            'latitud',
            'longitud',
            [
                'attribute'=>'ruta',
                'value' => function ($model) {
                    return $model->ruta->nombre;
                },
            ], 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
