<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Categoriavoto */

$this->title = 'Crear Categoría de voto';
$this->params['breadcrumbs'][] = ['label' => 'Categorías de votos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categoriavoto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
