<?php
use yii\helpers\Url;
use app\models\Lugar;

$lugarFactory = new Lugar();

$que = Yii::$app->request->get('que');
$donde = Yii::$app->request->get('donde');
$texto = Yii::$app->request->get('texto');

$lugares = $lugarFactory->filtrar($que,$donde,$texto);
?>

<meta name="viewport" content="initial-scale=1.0, width=device-width" />
<div id="map">
</div>
<style>
    .filtros{
        background:rgba(255,255,255,0.9);
        border-radius:5px;
        position:absolute;
        top:60px;
        left:10px;
        padding-right: 3px;
        font-size:9pt;
    }
    .imagen{
        padding:3px;
        height:35px;
    }
    .chb{
        position:relative;
        top:3px;
    }
    .chica{
        width:200px;
    }
    .linea{
        width:200px;
        height:5px;
        margin-top:5px;
    }
    .link{
        color:black;
    }
    .link:hover{
        text-decoration:none;
    }
</style>
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
<script>
    
var height = window.innerHeight - 110;
var mapa = document.getElementById("map");
mapa.style.height = height + "px";

<?php 



$lat = -40.7;
$lng = -73.1353;
$zoom = 10;

?>
var lat = <?=$lat?>;
var lng = <?=$lng;?>;


var platform = new H.service.Platform({
    'apikey': 'N-YZ6I_nuKSPWOfumH-EimqlpC82j0uM_4bzmhEEpnw'
});
var defaultLayers = platform.createDefaultLayers();
var map = new H.Map(document.getElementById('map'),
    defaultLayers.vector.normal.map,{
        center: {lat:lat, lng:lng},
        zoom: <?=$zoom?>,
        pixelRatio: window.devicePixelRatio || 1
    }
);
var ui = H.ui.UI.createDefault(map, defaultLayers);
window.addEventListener('resize', () => map.getViewPort().resize());
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));


function addMarkersToMap(map) {
    var group = new H.map.Group();
    group.addEventListener('tap', function (evt) {
        var burbujas = ui.getBubbles();    
        for(var i=0;i<burbujas.length;i++){
            ui.removeBubble(burbujas[i]);
        }
        var bubble =  new H.ui.InfoBubble(evt.target.getGeometry(), {
            content: evt.target.getData()
        });
        ui.addBubble(bubble);
    }, false);
    <?php
    foreach($lugares as $lugar):
    ?>
        var icon = new H.map.Icon("<?=Yii::getAlias("@web")."/images/iconos/1/".$lugar->tipo_id?>.png");        
        var lugar = new H.map.Marker({
            lat: '<?=$lugar->latitud;?>',
            lng: '<?=$lugar->longitud;?>'
        },{
            icon: icon
        });

        var html = "<a class='link' href='<?=Url::to(['lugar/view', 'id' => $lugar->id]);?>'>"+
            "<img class='chica' src='<?=Yii::getAlias("@web")?>/images/lugares/<?=$lugar->id?>/chica.jpg'><br/>"+
            "<img class='linea' src='<?=Yii::getAlias("@web")?>/images/linea.png'><br/>"+
            "<p style='width:200px;text-align:center'><b><?=Lugar::purify($lugar->nombre)?></b><br/><?=Lugar::purify($lugar->direccion)?></p>"+
            "</a>";

        lugar.setData(html);
        group.addObject(lugar);    
    <?php
    endforeach;
    ?>
    map.addObject(group);
}


window.onload = function () {
    addMarkersToMap(map);   
}; 


</script>