<?php
use yii\helpers\Url;
use app\models\Lugar;

$filterStr = yii\helpers\HtmlPurifier::process(Yii::$app->getRequest()->getQueryParam('filter'));
$lastChar = substr($filterStr,strlen($filterStr)-1,1);
if(strlen($filterStr)>0 && $lastChar == ","){
    $filterStr = substr($filterStr,0,strlen($filterStr)-1);
}
$filters = [];
if($filterStr!=null){
    $filters = explode(",",$filterStr);
}
$lugares = $ruta->getLugares($filters);
?>

<meta name="viewport" content="initial-scale=1.0, width=device-width" />

<div class='filtros'>
<?php
$tipos = app\models\Tipo::find()->all();

foreach($tipos as $tipo):
    $checked = "";
    if(in_array($tipo->id,$filters) || count($filters)==0){
        $checked = "checked";
    }
?>
    <div class='row'>
        <span>
            <img class='imagen' src='<?=Yii::getAlias("@web")."/images/iconos/".$ruta->id."/".$tipo->id?>_.png'/>
        </span>
        <span><input <?=$checked?> class='chb' type='checkbox' id='<?=$tipo->id?>'/></span>
        <span>
            <?=$tipo->nombre;?>
        </span>
    </div>
<?php endforeach;?>
</div>
<div id="map">
</div>
<link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-core.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-service.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-ui.js"></script>
    <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
<script>
    
var height = window.innerHeight - 110;
var mapa = document.getElementById("map");
mapa.style.height = height + "px";


<?php 
$url = Url::to(['site/mapa', 'id' => $ruta->id]);
$script = <<< JS
        
$(".chb").on("click", function(e, item) {
    var filter = "";
    $(".chb").each(function() {
        var checked = $(this).prop('checked');
        var id = $(this).attr('id');
        if(checked){
            filter += id+",";
        }
    });      
    window.location = '$url'+'&filter=' + filter;
});

var filtroHeight = $('.filtros').height();
$('#map').css("margin-top",'-'+filtroHeight+"px ");        

JS;
$this->registerJs($script);




$lat = -40.7;
$lng = -73.1353;
$zoom = 10;

?>
var lat = <?=$lat?>;
var lng = <?=$lng;?>;


var platform = new H.service.Platform({
    'apikey': 'N-YZ6I_nuKSPWOfumH-EimqlpC82j0uM_4bzmhEEpnw'
});
var defaultLayers = platform.createDefaultLayers();
var map = new H.Map(document.getElementById('map'),
    defaultLayers.vector.normal.map,{
        center: {lat:lat, lng:lng},
        zoom: <?=$zoom?>,
        pixelRatio: window.devicePixelRatio || 1
    }
);
var ui = H.ui.UI.createDefault(map, defaultLayers);
window.addEventListener('resize', () => map.getViewPort().resize());
var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));


<?php
$puntos = $ruta->getPuntosOrdenados();

if(count($puntos)>=2):
?>
var routingParameters = {
    'mode': 'shortest;car',
    <?php 
    $i=0;
    foreach($puntos as $punto):?>
    'waypoint<?=$i?>': 'geo!<?=$punto->latitud?>,<?=$punto->longitud?>',
    <?php 
    $i++;
    endforeach;?>
    'representation': 'display'
};

var onResult = function(result) {
    var route,
    routeShape,
    startPoint,
    endPoint,
    linestring;
    if(result.response.route) {
        route = result.response.route[0];
        routeShape = route.shape;
        linestring = new H.geo.LineString();
        routeShape.forEach(function(point) {
            var parts = point.split(',');
            linestring.pushLatLngAlt(parts[0], parts[1]);
        });

        startPoint = route.waypoint[0].mappedPosition;
        endPoint = route.waypoint[1].mappedPosition;

        var routeLine = new H.map.Polyline(linestring, {
            style: { strokeColor: '<?=$ruta->color?>', lineWidth: 2 }
        });
        
        map.addObjects([routeLine]);
    }
};

var router = platform.getRoutingService();
router.calculateRoute(routingParameters, onResult,
    function(error) {
        alert(error.message);
});

<?php
endif;
?>

function addMarkersToMap(map) {
    var group = new H.map.Group();
    group.addEventListener('tap', function (evt) {
        var burbujas = ui.getBubbles();    
        for(var i=0;i<burbujas.length;i++){
            ui.removeBubble(burbujas[i]);
        }
        var bubble =  new H.ui.InfoBubble(evt.target.getGeometry(), {
            content: evt.target.getData()
        });
        ui.addBubble(bubble);
    }, false);
    <?php
    foreach($lugares as $lugar):
    ?>
        var icon = new H.map.Icon("<?=Yii::getAlias("@web")."/images/iconos/".$ruta->id."/".$lugar->tipo_id?>.png");        
        var lugar = new H.map.Marker({
            lat: '<?=$lugar->latitud;?>',
            lng: '<?=$lugar->longitud;?>'
        },{
            icon: icon
        });

        var html = "<a class='link' href='<?=Url::to(['lugar/view', 'id' => $lugar->id]);?>'>"+
            "<img class='chica' src='<?=Yii::getAlias("@web")?>/images/lugares/<?=$lugar->id?>/chica.jpg'><br/>"+
            "<img class='linea' src='<?=Yii::getAlias("@web")?>/images/linea.png'><br/>"+
            "<p style='width:200px;text-align:center'><b><?=Lugar::purify($lugar->nombre)?></b><br/><?=Lugar::purify($lugar->direccion)?></p>"+ 
            "</a>";

        lugar.setData(html);
        group.addObject(lugar);    
    <?php
    endforeach;
    ?>
    map.addObject(group);
}


window.onload = function () {
    addMarkersToMap(map);   
}; 


</script>