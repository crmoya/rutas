<table class="rotado">
    <tr>
        <td class="transparente"></td>
        <td id="1" color="#1a4c7e" class="cuadro imagen1" ruta="RUTA DE CORDILLERA AL MAR" nombre="ALERCES"></td>
        <td id="2" color="#107b6f" class="cuadro imagen2" ruta="RUTA INTERLAGOS Y VOLCANES" nombre="ESCULTURA"></td>
        <td class="transparente"></td>
    </tr>
    <tr>
        <td id="3" color="#ae4143" class="cuadro imagen3" ruta="RUTA DE LOS ORÍGENES" nombre="MANQUEMAPU"></td>
        <td id="4" color="#ae4143" class="cuadro imagen4" ruta="RUTA DE LOS ORÍGENES" nombre="MANQUEMAPU"></td>
        <td id="5" color="#ae4143" class="cuadro imagen5" ruta="RUTA INTERLAGOS Y VOLCANES" nombre="PAMPA BONITA"></td>
        <td id="6" color="#1a4c7e" class="cuadro imagen6" ruta="RUTA DE CORDILLERA AL MAR" nombre="PANGUIRUKA"></td>
    </tr>
    <tr>
        <td id="7" color="#ae4143" class="cuadro imagen7" ruta="RUTA DE LOS ORÍGENES" nombre="PLAZA PURRANQUE"></td>
        <td class="transparente actividades"></td>
        <td id="8" color="#ae4143" class="cuadro imagen8" ruta="RUTA DE LOS ORÍGENES" nombre="SAN PEDRO"></td>
        <td id="9" color="#ae4143" class="cuadro imagen9" ruta="RUTA DE LOS ORÍGENES" nombre="SAN PEDRO"></td>
    </tr>
    <tr>
        <td class="transparente"></td>
        <td id="10" color="#ae4143" class="cuadro imagen10" ruta="RUTA DE LOS ORÍGENES" nombre="SAN PEDRO"></td>
        <td id="11" color="#ae4143" class="cuadro imagen11" ruta="RUTA DE LOS ORÍGENES" nombre="COÑICO"></td>
        <td id="12" color="#107b6f" class="cuadro imagen12" ruta="RUTA INTERLAGOS Y VOLCANES" nombre="TEATRO"></td>
    </tr>
</table>
<div class="lienzo" style="display:none;">
    <div class="x">X</div>
    <div class="marco"></div>
    <img src="<?=Yii::getAlias("@web")?>/images/linea.png" class="linea">
    <div class="nombre"></div>
</div>
<div class="telon" style="display:none;"></div>
<?php

$url = Yii::getAlias("@web");
$script = <<< JS
        
var showing = false;
var height = $(window).height();
var width = $(window).width();
$(document).ready(function(e) {    
    $('.cuadro').height((height)/4); 
    $('.cuadro').width((height)/4);  
    $('.rotado').css('margin-left',(height/4)+'px');
    $('.rotado').css('margin-top','50px');
    $('.telon').height(height);
    $('.telon').width(width);
    
    $('.lienzo').height(height-10);    
});

$('.cuadro').click(function(e){
    var id = $(this).attr('id');
    var nombre = $(this).attr('nombre');
    var color = $(this).attr('color');
    var ruta = $(this).attr('ruta');
    lightbox(id,nombre,color,ruta);
});
$('.telon').click(function(e){
    lightbox(-1,'','#ffffff','');
});
$('.x').click(function(e){
    lightbox(-1,'','#ffffff','');
});

function lightbox(id,nombre,color,ruta){
    $('.nombre').html("");
    $('.telon').css("background-color",color);
    if(!showing){
        $('.lienzo').height(0);
        $('.lienzo').width(0);
        $('.lienzo').show();
        showing = true;
    }
    else{
        $('.telon').hide();
        $('.lienzo').hide();
        showing = false;
    }
    if(id>0){
        $('.marco').empty();
        var img = new Image();
        $(img).on('load',function(){
            $('.marco').append($(this));
            var alto = $(this).height();
            if(alto > height - 80){
                $(this).height(height-80);
            }
            var ancho = $(this).width();
            if(ancho > width - 80){
                $(this).width(width-80);
            }
            alto = $(this).height();
            ancho = $(this).width();

            var x = (width - ancho - 20)/2;
            var y = (height - alto - 60)/2;
            $('.lienzo').width(ancho+20);
            $('.linea').width(ancho);
            $('.linea').css('left','10px');
            $('.lienzo').height(alto+60);
            $('.x').css('left',ancho+'px');
            $('.lienzo').css('left',x+'px');
            $('.lienzo').css('top',y+'px');  
            $('.nombre').html(nombre+" - "+ruta);     
            $('.telon').show();
        }).attr({
            src: '$url/images/galeria/'+id+'.jpg',
        });
        
    }
}


JS;
$this->registerJs($script);
?>

