<?php

/* @var $this yii\web\View */

use yii\widgets\ActiveForm;

use app\models\Ruta;
use \yii\helpers\Url;
use \yii\helpers\ArrayHelper;
use app\models\Ciudad;
use app\models\Tipo;
use \yii\helpers\Html;

$this->title = Yii::$app->name;

?>  
<div class="rutas-ch ch">
    <?php $form = ActiveForm::begin([
        'id' => 'index-form',
    ]); ?>
        <div class="busca-ch">
            <p class="letra-busca-ch">
                Busca una experiencia
            </p>
            <div class="donde">
            <?php
                $ciudades=ArrayHelper::map(Ciudad::find()->asArray()->all(), 'id', 'nombre');
                echo $form->field($model, 'donde')->dropDownList($ciudades,['class'=>'form-control','prompt'=>'Seleccione Dónde']);
            ?>
            </div>
            <div class="que">
            <?php
                $tipos=ArrayHelper::map(Tipo::find()->asArray()->all(), 'id', 'nombre');
                echo $form->field($model, 'que')->dropDownList($tipos,['class'=>'form-control','prompt'=>'Seleccione Qué']);
            ?>
            </div>
            <b>BUSCAR</b>
            <div class="row ch">
                <?=$form->field($model, 'texto')->textInput(['class'=>'col-xs-9 form-control','style'=>'width:70%;'])->label(false);?>
                <?= Html::submitButton('Buscar', ['class' => 'col-xs-3 btn btn-danger','style'=>'width:28%;margin-top:-5px;margin-left:5px;']) ?>
            </div>
        </div>
    <?php 
    ActiveForm::end(); 
    ?>
</div>
<div class="row ch">
    <?php
    $rutas = Ruta::find()->all();
    foreach($rutas as $ruta):?>
    <div class="col-xs-4 img-ruta-ch">
        <img class="ruta ruta-img" ruta="<?=$ruta->id?>" src="<?=Yii::getAlias("@web")?>/images/<?=$ruta->id?>_.png"/>
    </div> 
    <?php endforeach;?>
</div>


<div class="rutas gr">
<?php $form = ActiveForm::begin([
        'id' => 'index-form',
    ]); ?>

    <div class="busca col-md-3">
        <p class="letra-busca">
            Busca una experiencia
        </p>
        <img width="280px" height="5px" src="<?=Yii::getAlias("@web")?>/images/linea.png"/>
        <div class="donde">
        <?php
            $ciudades=ArrayHelper::map(Ciudad::find()->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'donde')->dropDownList($ciudades,['class'=>'form-control','prompt'=>'Seleccione Dónde']);
        ?>
        </div>
        <div class="que">
        <?php
            $tipos=ArrayHelper::map(Tipo::find()->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'que')->dropDownList($tipos,['class'=>'form-control','prompt'=>'Seleccione Qué']);
        ?>
        </div>
        <p><b>BUSCAR</b></p>
        <div>
            <?=$form->field($model, 'texto')->label(false);?>
            <?= Html::submitButton('', ['style' => 'position:relative;top:-40px;left:250px;border:none;height:30px;width:20px;background:no-repeat url("'.Yii::getAlias("@web").'/images/lupa.png")']) ?>
        </div>
    </div>
    <div class="col-md-1">&nbsp;</div>
    <div class="col-md-8 img-rutas">
        <div class="row">
            <?php
            $rutas = Ruta::find()->all();
            foreach($rutas as $ruta):?>
            <div class="col-md-4">
                <img class="ruta" ruta="<?=$ruta->id?>" src="<?=Yii::getAlias("@web")?>/images/<?=$ruta->id?>.png"/>
            </div> 
            <?php endforeach;?>
        </div>
    </div>


<?php 
ActiveForm::end(); 
?>
</div>

<div class="rutas md">
<?php $form = ActiveForm::begin([
        'id' => 'index-form',
    ]); ?>


    <div class="row">
        <?php
        $rutas = Ruta::find()->all();
        foreach($rutas as $ruta):?>
        <div class="col-xs-4">
            <img class="ruta" ruta="<?=$ruta->id?>" src="<?=Yii::getAlias("@web")?>/images/<?=$ruta->id?>.png"/>
        </div> 
        <?php endforeach;?>
    </div>
    <div class="row" style="text-align:center;">
        <div class="col-xs-3"></div>
        <div class="busca col-xs-6">
            <p class="letra-busca">
                Busca una experiencia
            </p>
            <img width="280px" height="5px" src="<?=Yii::getAlias("@web")?>/images/linea.png"/>
            <div class="donde">
            <?php
                $ciudades=ArrayHelper::map(Ciudad::find()->asArray()->all(), 'id', 'nombre');
                echo $form->field($model, 'donde')->dropDownList($ciudades,['class'=>'form-control','prompt'=>'Seleccione Dónde']);
            ?>
            </div>
            <div class="que">
            <?php
                $tipos=ArrayHelper::map(Tipo::find()->asArray()->all(), 'id', 'nombre');
                echo $form->field($model, 'que')->dropDownList($tipos,['class'=>'form-control','prompt'=>'Seleccione Qué']);
            ?>
            </div>
            <p><b>BUSCAR</b></p>
            <div>
                <?=$form->field($model, 'texto')->label(false);?>
                <?= Html::submitButton('', ['style' => 'position:relative;top:-40px;left:250px;border:none;height:30px;width:20px;background:no-repeat url("'.Yii::getAlias("@web").'/images/lupa.png")']) ?>
            </div>
        </div>
    </div>

<?php 
ActiveForm::end(); 
?>
</div>



<?php
$url = Url::to(['site/mapa']);

$script = <<< JS

$('.ruta').click(function(e){
    var url = '$url?id='+$(this).attr('ruta');
    window.location = url;
});
        
JS;

$this->registerJs($script);


?>
