<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Usuario;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Emprendedor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emprendedor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?php     
    $usuarios = (new Usuario())->findWithNombre();
    $usuarios =ArrayHelper::map($usuarios, 'id', 'nombre'); ?>
    <?= $form->field($model, 'usuario_id')->dropDownList($usuarios,['prompt'=>'Seleccione Usuario']); ?>
    
    <div class="form-group">
        <?= Html::submitButton('Grabar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
