<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Emprendedor */

$this->title = 'Crear Emprendedor';
$this->params['breadcrumbs'][] = ['label' => 'Emprendedors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emprendedor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
