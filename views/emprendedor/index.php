<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmprendedorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emprendedores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emprendedor-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Emprendedor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'telefono',
            [
                'attribute'=>'usuario',
                'value' => function ($model) {
                    return $model->usuario->nombre." ".$model->usuario->apellido;
                },
            ], 

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
