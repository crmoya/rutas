<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Ruta;
use app\models\Lugar;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Pertenece */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pertenece-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php $lugares=ArrayHelper::map(Lugar::find()->asArray()->all(), 'id', 'nombre'); ?>
    <?= $form->field($model, 'lugar_id')->dropDownList($lugares,['prompt'=>'Seleccione Lugar']); ?>

    <?php $rutas=ArrayHelper::map(Ruta::find()->asArray()->all(), 'id', 'nombre'); ?>
    <?= $form->field($model, 'ruta_id')->dropDownList($rutas,['prompt'=>'Seleccione Ruta']); ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
