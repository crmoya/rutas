<?php

namespace app\controllers;

use app\models\BuscaForm;
use app\models\Ruta;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Lugar;

class SiteController extends Controller
{

    public $layout = '//site';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new BuscaForm();
        if ($model->load(Yii::$app->request->post())) {
            return $this->redirect(['site/mapa-buscar', 
            'que' => (int)$model->que,
            'donde' => (int)$model->donde,
            'texto' => \yii\helpers\HtmlPurifier::process($model->texto)]);
        }
        return $this->render('index',['model'=>$model]);
    }

    public function actionGaleria(){
        $this->layout = "mapa";
        return $this->render('galeria');
    }
    
    public function actionMapa($id)
    {
        $this->layout = "mapa";
        $model = Ruta::findOne($id);
        if(isset($model)){
            return $this->render('mapa',['ruta'=>$model]);   
        }
        else{
            return $this->redirect(['site/index']);
        }
    }

    public function actionMapaBuscar()
    {
        $this->layout = "mapa";
        return $this->render('mapa-buscar');   
    }

    public function actionLogin(){
        return $this->redirect(['usuario/login']);
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
