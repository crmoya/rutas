<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellido
 * @property string $correo
 * @property string $clave
 * @property string $auth_key 
 *
 * @property Emprendedor[] $emprendedors
 * @property Vota[] $votas
 */
class Usuario extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'correo', 'clave'], 'required'],
            [['nombre', 'apellido', 'correo'], 'string', 'max' => 100],
            [['clave'], 'string', 'max' => 60],
            [['correo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'correo' => 'Correo',
            'clave' => 'Clave',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmprendedors()
    {
        return $this->hasMany(Emprendedor::className(), ['usuario_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotas()
    {
        return $this->hasMany(Vota::className(), ['usuario_id' => 'id']);
    }
    
    public static function findIdentity($id)
    {
        return self::findOne($id)!=null ? new static(self::findOne($id)) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }
    
    public static function findByUsername($username)
    {
        return static::findOne(['correo' => $username]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function findWithNombre()
    {
        $usuarios = [];
        $users = Usuario::find()->all();
        foreach($users as $user)
        {
            $usuarios[] = ['id'=>$user->id,'nombre'=>$user->nombre." ".$user->apellido];
        }
        return $usuarios; 
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
    
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->clave);
    }
}
