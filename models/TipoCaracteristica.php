<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_caracteristica".
 *
 * @property int $id
 * @property int $tipo_id
 * @property int $caracteristica_id
 *
 * @property Caracteristica $caracteristica
 * @property Tipo $tipo
 */
class TipoCaracteristica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_caracteristica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_id', 'caracteristica_id'], 'required'],
            [['tipo_id', 'caracteristica_id'], 'integer'],
            [['caracteristica_id'], 'exist', 'skipOnError' => true, 'targetClass' => Caracteristica::className(), 'targetAttribute' => ['caracteristica_id' => 'id']],
            [['tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_id' => 'Tipo ID',
            'caracteristica_id' => 'Caracteristica ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaracteristica()
    {
        return $this->hasOne(Caracteristica::className(), ['id' => 'caracteristica_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_id']);
    }
}
