<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lugar".
 *
 * @property int $id
 * @property string $nombre
 * @property int $ciudad_id
 * @property int $tipo_id
 * @property string $descripcion
 * @property double $latitud
 * @property double $longitud
 * @property string $facebook
 * @property string $instagram
 * @property string $web
 *
 * @property Ciudad $ciudad
 * @property Tipo $tipo
 * @property LugarCaracteristica[] $lugarCaracteristicas
 * @property Posee[] $posees
 * @property Vota[] $votas
 * @property Votacion[] $votacions
 */
class Lugar extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lugar';
    }

    public static function purify($value){
        $value = str_replace("'","",$value);
        $value = str_replace('"',"",$value);
        $value = str_replace("\\","",$value);
        return $value;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'ciudad_id', 'tipo_id'], 'required'],
            [['ciudad_id', 'tipo_id'], 'integer'],
            [['descripcion','facebook','web','instagram'], 'string'],
            [['latitud', 'longitud'], 'number'],
            [['nombre'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
            [['ciudad_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudad::className(), 'targetAttribute' => ['ciudad_id' => 'id']],
            [['tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'ciudad_id' => 'Ciudad',
            'tipo_id' => 'Tipo',
            'descripcion' => 'Descripción',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudad::className(), ['id' => 'ciudad_id']);
    }

    public function filtrar($que,$donde,$texto){
        $query = Lugar::find();
        if($que > 0){
            $query->andFilterWhere(['tipo_id'=>$que]);
        }
        if($donde > 0){
            $query->andFilterWhere(['ciudad_id'=>$donde]);
        }
        if(strlen($texto) > 0){
            $query->andFilterWhere([
                'or',
                ['like', 'descripcion', $texto],
                ['like', 'nombre', $texto]
            ]);
        }
        return $query->all();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLugarCaracteristicas()
    {
        return $this->hasMany(LugarCaracteristica::className(), ['lugar_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosees()
    {
        return $this->hasMany(Posee::className(), ['lugar_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotas()
    {
        return $this->hasMany(Vota::className(), ['lugar_id' => 'id']);
    }

    public function getRutas(){
        $perteneces = $this->hasMany(Pertenece::className(), ['lugar_id' => 'id']);
        $dev = "";
        $i = 1;
        foreach($perteneces->all() as $pertenece){
            $dev .= $i.". ".$pertenece->ruta->nombre." ";
            $i++;
        }
        return $dev;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotacions()
    {
        return $this->hasMany(Votacion::className(), ['lugar_id' => 'id']);
    }
}
