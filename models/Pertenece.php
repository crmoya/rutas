<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pertenece".
 *
 * @property int $id
 * @property int $lugar_id
 * @property int $ruta_id
 *
 * @property Lugar $lugar
 * @property Ruta $ruta
 */
class Pertenece extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pertenece';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lugar_id', 'ruta_id'], 'required'],
            [['lugar_id', 'ruta_id'], 'integer'],
            [['lugar_id', 'ruta_id'], 'unique', 'targetAttribute' => ['lugar_id', 'ruta_id']],
            [['lugar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lugar::className(), 'targetAttribute' => ['lugar_id' => 'id']],
            [['ruta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ruta::className(), 'targetAttribute' => ['ruta_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lugar_id' => 'Lugar ID',
            'ruta_id' => 'Ruta ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLugar()
    {
        return $this->hasOne(Lugar::className(), ['id' => 'lugar_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuta()
    {
        return $this->hasOne(Ruta::className(), ['id' => 'ruta_id']);
    }
}
