<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruta".
 *
 * @property int $id
 * @property string $nombre
 * @property string $color
 *
 * @property Pertenece[] $perteneces
 */
class Ruta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'color'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['color'], 'string', 'max' => 10],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'color' => 'Color',
        ];
    }

    public function getPuntos(){
        return $this->hasMany(Punto::className(), ['ruta_id' => 'id']);
    }

    public function getPuntosOrdenados(){
        return Punto::find()->where(['ruta_id'=>$this->id])->orderBy(['orden' => SORT_ASC])->all();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::className(), ['ruta_id' => 'id']);
    }
    
    public function getLugares($tipos){
        $perteneces = $this->perteneces;
        $lugares = [];
        foreach($perteneces as $pertenece){
            if(count($tipos)<1){
                $lugares[] = $pertenece->lugar;
            }
            else{
                if(in_array($pertenece->lugar->tipo_id,$tipos)){
                    $lugares[] = $pertenece->lugar;
                }
            }
        }
        return $lugares;
    }
}
