<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emprendedor".
 *
 * @property int $id
 * @property string $telefono
 * @property int $usuario_id
 *
 * @property Usuario $usuario
 * @property Posee[] $posees
 */
class Emprendedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'emprendedor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id'], 'required'],
            [['usuario_id'], 'integer'],
            [['telefono'], 'string', 'max' => 45],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'telefono' => 'Teléfono',
            'usuario_id' => 'Usuario',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosees()
    {
        return $this->hasMany(Posee::className(), ['emprendedor_id' => 'id']);
    }

    public function findUsuarios()
    {
        $emprendedores = Emprendedor::find()->all();
        $usuarios = [];
        foreach($emprendedores as $emprendedor){
            $usuarios[] = ['id'=>$emprendedor->id,'nombre'=>$emprendedor->usuario->nombre." ".$emprendedor->usuario->apellido];
        }
        return $usuarios;
    }
}
