<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categoriavoto".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property TipoCategoriavoto[] $tipoCategoriavotos
 * @property Votacion[] $votacions
 */
class Categoriavoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categoriavoto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCategoriavotos()
    {
        return $this->hasMany(TipoCategoriavoto::className(), ['categoriavoto_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVotacions()
    {
        return $this->hasMany(Votacion::className(), ['categoriavoto_id' => 'id']);
    }
}
