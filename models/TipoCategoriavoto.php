<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_categoriavoto".
 *
 * @property int $id
 * @property int $tipo_id
 * @property int $categoriavoto_id
 *
 * @property Categoriavoto $categoriavoto
 * @property Tipo $tipo
 */
class TipoCategoriavoto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_categoriavoto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_id', 'categoriavoto_id'], 'required'],
            [['tipo_id', 'categoriavoto_id'], 'integer'],
            [['categoriavoto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categoriavoto::className(), 'targetAttribute' => ['categoriavoto_id' => 'id']],
            [['tipo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tipo::className(), 'targetAttribute' => ['tipo_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tipo_id' => 'Tipo ID',
            'categoriavoto_id' => 'Categoriavoto ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriavoto()
    {
        return $this->hasOne(Categoriavoto::className(), ['id' => 'categoriavoto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipo()
    {
        return $this->hasOne(Tipo::className(), ['id' => 'tipo_id']);
    }
}
