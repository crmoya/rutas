<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Punto;

/**
 * PuntoSearch represents the model behind the search form of `app\models\Punto`.
 */
class PuntoSearch extends Punto
{
    public $ruta;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'orden', 'ruta_id'], 'integer'],
            [['ruta'], 'safe'],
            [['latitud', 'longitud'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Punto::find();
        $query->joinWith(['ruta']);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['ruta'] = [
            'asc' => ['ruta.nombre' => SORT_ASC],
            'desc' => ['ruta.nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'orden' => $this->orden,
            'latitud' => $this->latitud,
            'longitud' => $this->longitud,
            'ruta_id' => $this->ruta_id,
        ]);
        
        $query
            ->andFilterWhere(['like', 'ruta.nombre', $this->ruta]);

        return $dataProvider;
    }
}
