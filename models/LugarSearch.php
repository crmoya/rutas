<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lugar;

/**
 * LugarSearch represents the model behind the search form of `app\models\Lugar`.
 */
class LugarSearch extends Lugar
{
    
    public $ciudad;
    public $tipo;
    public $rutas;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ciudad_id', 'tipo_id'], 'integer'],
            [['nombre', 'descripcion','ciudad','tipo'], 'safe'],
            [['latitud', 'longitud'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Lugar::find();
        $query->joinWith(['ciudad','tipo']);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['ciudad'] = [
            'asc' => ['ciudad.nombre' => SORT_ASC],
            'desc' => ['ciudad.nombre' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['tipo'] = [
            'asc' => ['tipo.nombre' => SORT_ASC],
            'desc' => ['tipo.nombre' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lugar.id' => $this->id,
            'ciudad_id' => $this->ciudad_id,
            'tipo_id' => $this->tipo_id,
            'latitud' => $this->latitud,
            'longitud' => $this->longitud,
        ]);

        $query
            ->andFilterWhere(['like', 'lugar.nombre', $this->nombre])
            ->andFilterWhere(['like', 'descripcion', $this->descripcion])
            ->andFilterWhere(['like', 'ciudad.nombre', $this->ciudad])
            ->andFilterWhere(['like', 'tipo.nombre', $this->tipo]);

        return $dataProvider;
    }
}
