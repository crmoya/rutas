<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "caracteristica".
 *
 * @property int $id
 * @property string $nombre
 *
 * @property LugarCaracteristica[] $lugarCaracteristicas
 * @property TipoCaracteristica[] $tipoCaracteristicas
 */
class Caracteristica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'caracteristica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLugarCaracteristicas()
    {
        return $this->hasMany(LugarCaracteristica::className(), ['caracteristica_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoCaracteristicas()
    {
        return $this->hasMany(TipoCaracteristica::className(), ['caracteristica_id' => 'id']);
    }
}
