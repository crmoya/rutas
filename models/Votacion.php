<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "votacion".
 *
 * @property int $id
 * @property int $valor
 * @property int $lugar_id
 * @property int $categoriavoto_id
 *
 * @property Categoriavoto $categoriavoto
 * @property Lugar $lugar
 */
class Votacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'votacion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['valor', 'lugar_id', 'categoriavoto_id'], 'required'],
            [['valor', 'lugar_id', 'categoriavoto_id'], 'integer'],
            [['categoriavoto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categoriavoto::className(), 'targetAttribute' => ['categoriavoto_id' => 'id']],
            [['lugar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lugar::className(), 'targetAttribute' => ['lugar_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'valor' => 'Valor',
            'lugar_id' => 'Lugar ID',
            'categoriavoto_id' => 'Categoriavoto ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriavoto()
    {
        return $this->hasOne(Categoriavoto::className(), ['id' => 'categoriavoto_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLugar()
    {
        return $this->hasOne(Lugar::className(), ['id' => 'lugar_id']);
    }
}
