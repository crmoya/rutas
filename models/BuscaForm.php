<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class BuscaForm extends Model
{
    public $donde;
    public $que;
    public $texto;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['que','donde'], 'integer'],
            [['texto'], 'string', 'max' => 200],
        ];
    }

    public function attributeLabels()
    {
        return [
            'donde' => 'DÓNDE',
            'que' => 'QUÉ HACER',
        ];
    }

}
