<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posee".
 *
 * @property int $id
 * @property int $lugar_id
 * @property int $emprendedor_id
 *
 * @property Emprendedor $emprendedor
 * @property Lugar $lugar
 */
class Posee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lugar_id', 'emprendedor_id'], 'required'],
            [['lugar_id', 'emprendedor_id'], 'integer'],
            [['emprendedor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Emprendedor::className(), 'targetAttribute' => ['emprendedor_id' => 'id']],
            [['lugar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lugar::className(), 'targetAttribute' => ['lugar_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lugar_id' => 'Lugar',
            'emprendedor_id' => 'Emprendedor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmprendedor()
    {
        return $this->hasOne(Emprendedor::className(), ['id' => 'emprendedor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLugar()
    {
        return $this->hasOne(Lugar::className(), ['id' => 'lugar_id']);
    }
}
