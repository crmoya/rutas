<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pertenece;

/**
 * PerteneceSearch represents the model behind the search form of `app\models\Pertenece`.
 */
class PerteneceSearch extends Pertenece
{
    
    public $lugar;
    public $ruta;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ruta','lugar'], 'safe'],
            [['id', 'lugar_id', 'ruta_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pertenece::find();
        $query->joinWith(['ruta','lugar']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['ruta'] = [
            'asc' => ['ruta.nombre' => SORT_ASC],
            'desc' => ['ruta.nombre' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['lugar'] = [
            'asc' => ['lugar.nombre' => SORT_ASC],
            'desc' => ['lugar.nombre' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lugar_id' => $this->lugar_id,
            'ruta_id' => $this->ruta_id,
        ]);
        
        $query
            ->andFilterWhere(['like', 'lugar.nombre', $this->lugar])
            ->andFilterWhere(['like', 'ruta.nombre', $this->ruta]);

        return $dataProvider;
    }
}
