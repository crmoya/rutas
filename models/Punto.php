<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "punto".
 *
 * @property int $id
 * @property int $orden
 * @property double $latitud
 * @property double $longitud
 * @property int $ruta_id
 *
 * @property Ruta $ruta
 */
class Punto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'punto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['orden', 'latitud', 'longitud', 'ruta_id'], 'required'],
            [['orden', 'ruta_id'], 'integer'],
            [['latitud', 'longitud'], 'number'],
            [['ruta_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ruta::className(), 'targetAttribute' => ['ruta_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'orden' => 'Orden',
            'latitud' => 'Latitud',
            'longitud' => 'Longitud',
            'ruta_id' => 'Ruta',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuta()
    {
        return $this->hasOne(Ruta::className(), ['id' => 'ruta_id']);
    }
}
