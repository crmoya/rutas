<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lugar_caracteristica".
 *
 * @property int $id
 * @property int $lugar_id
 * @property int $caracteristica_id
 * @property string $valor
 *
 * @property Caracteristica $caracteristica
 * @property Lugar $lugar
 */
class LugarCaracteristica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lugar_caracteristica';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lugar_id', 'caracteristica_id', 'valor'], 'required'],
            [['lugar_id', 'caracteristica_id'], 'integer'],
            [['valor'], 'string', 'max' => 100],
            [['caracteristica_id'], 'exist', 'skipOnError' => true, 'targetClass' => Caracteristica::className(), 'targetAttribute' => ['caracteristica_id' => 'id']],
            [['lugar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Lugar::className(), 'targetAttribute' => ['lugar_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lugar_id' => 'Lugar ID',
            'caracteristica_id' => 'Caracteristica ID',
            'valor' => 'Valor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaracteristica()
    {
        return $this->hasOne(Caracteristica::className(), ['id' => 'caracteristica_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLugar()
    {
        return $this->hasOne(Lugar::className(), ['id' => 'lugar_id']);
    }
}
